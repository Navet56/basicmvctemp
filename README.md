# MVC Basic Template

This Java program allows to have an MVC pattern to code a Swing application

## Model

Where the data and functional classes and interfaces are.

## View

Where the Graphical User Interface is. In Swing here.

## Control

The Controller allows seeing what the user do with the view and change the models in consequence.

## Core

The program starts here.

## How to run this template ?

* With the script : `run.sh`
* Or with IntelliJ IDEA : Run -> Run 'Main'
* Or with the command line : <br/>
` javac model/*.java view/*.java control/*.java core/*.java `  <br/>
`java core/Main`

(you can add arguments, the args must be showed in the window)
