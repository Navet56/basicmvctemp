package core;

import control.Controller;
import model.Model;
import javax.swing.SwingUtilities;

/**
 * Main class to launch the program
 * @author Navet56
 */
public class Main {
    /**
     * Allows to initialize the program (the models, the view and the controller)
     * @param args the main args
     */
    public static void initUI(String[] args){
        Model model;
        if (args == null) model = new Model();
        else model = new Model(args);
        Controller control = new Controller(model);
        control.showGUI();
    }

    /**
     * Main method
     * Uses SwingUtilities.invokeLater() to be thread safe
     * @param args the arguments that we can type during the program launch (<code>java Main arg1 arg2 ...</code>)
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (args.length != 0) initUI(args);
                else initUI(null);
            }
        });
    }
}

