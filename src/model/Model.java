package model;

/**
 * Model main class
 * with the all Models fields
 */
public class Model {
    public static final String VERSION = "1.0";
    public static final String TITLE = "Basic MVC Template in Java Swing";
    public static final String AUTHOR = "Navet56";
    public static final String LICENSE = "GNU GPL v3";

    private String data;

    public Model(){
        this.data = "void";
    }

    public Model(String[] args){
        this.data = "";
        for (String arg : args) {
            this.data = this.data + " " + arg;
        }
    }

    public String getData() {
        return data;
    }
}
