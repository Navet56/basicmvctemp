package view;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {

    private JLabel text;

    public MainPanel() {
        this.setLayout(new GridLayout(1,1));
        this.text = new JLabel(" Welcome to the MVC Swing basic template !");
        this.text.setHorizontalAlignment(JLabel.CENTER);
        this.add(this.text);
    }

    public JLabel getLabel() {
        return this.text;
    }
}
