package view;
import model.Model;

/**
 * Main View class
 * with all JFrames
 */
public class View {

    private Model model;
    private MainFrame mainFrame;
    private AboutFrame aboutFrame;

    public View(Model model){
        this.model = model;
    }

    /**
     * Allows to init the JFrames
     */
    public void start(){
        this.mainFrame = new MainFrame(model.TITLE);
    }

    public MainFrame getMainFrame() {
        return this.mainFrame;
    }

    public AboutFrame getAboutFrame() {
        return aboutFrame;
    }

    public Model getModel() {
        return this.model;
    }
}
