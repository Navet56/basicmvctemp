package view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.HeadlessException;

/**
 * Classic About window with the App informations
 */
public class AboutFrame extends JFrame {

    private JLabel devLabel;
    private JLabel verLabel;
    private JLabel licLabel;

    /**
     * AboutFrame constructor
     * @param dev Dev information
     * @param version
     * @param license the code license (like GPL, MIT etc)
     * @throws HeadlessException
     */
    public AboutFrame(String dev, String version, String license) throws HeadlessException {
        super("About this software");
        this.setLayout(new GridLayout(3,1));

        this.devLabel = new JLabel(dev);
        this.devLabel.setHorizontalAlignment(JLabel.CENTER);
        this.add(devLabel);
        this.verLabel = new JLabel("Version " + version);
        this.verLabel.setHorizontalAlignment(JLabel.CENTER);
        this.add(verLabel);
        this.licLabel = new JLabel(license);
        this.licLabel.setHorizontalAlignment(JLabel.CENTER);
        this.add(licLabel);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setSize(400,150);
    }

}
