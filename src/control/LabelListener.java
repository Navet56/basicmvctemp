package control;

import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Generic Listener to change a JLabel text
 */
public class LabelListener implements ActionListener {

    private JLabel label;
    private String newText;

    /**
     * LabelListener constructor
     * @param label the JLabel object
     * @param newText the new text String
     */
    public LabelListener(JLabel label, String newText){
        this.label = label;
        this.newText = newText;
    }

    /**
     * This method changes the JLabel text
     * @param e the event
     */
    public void actionPerformed(ActionEvent e) {
        this.label.setText(this.newText);
    }
}
