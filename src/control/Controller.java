package control;

import view.*;
import model.*;

/**
 * Generic Controller class
 */
public class Controller {
    private View view ;
    private Model model;

    /**
     * Controller constructor
     * This constructor creates a new View instance
     * @param model a generic Model
     */
    public Controller(Model model) {
        if (model != null){
          this.model = model;
          this.view = new View(this.model);
        }
        else System.out.println("Controller() : model is null");
    }

    /**
     * This method initializes the GUI and set the different listeners
     */
    public void showGUI(){
        this.view.start();

        //Menu Listeners definition
        MainMenu mainMenu = this.view.getMainFrame().getMenu();
        mainMenu.getQuit().addActionListener(new QuitListener());
        mainMenu.getAbout().addActionListener( e -> new AboutFrame(Model.TITLE + " by " + Model.AUTHOR, Model.VERSION, Model.LICENSE)); //this is a lambda function, see more in docs.oracle.com
        mainMenu.getNewFile().addActionListener(new LabelListener(this.view.getMainFrame().getMainPanel().getLabel(), this.view.getModel().getData()));

        //Others listeners :

        //void
    }
}
